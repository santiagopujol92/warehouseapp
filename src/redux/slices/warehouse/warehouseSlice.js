import credentials from '../../../credentials'
import { createSlice } from '@reduxjs/toolkit'
import { WAREHOUSE_INITIALDATA } from './warehouseInitialData'
import { updateLocalStorage, getDataLocalStorage } from '../../localStorage'
const entityKey = 'warehouses';

export const wareHouseSlice = createSlice({
  name: 'warehouse',
  initialState: {
    list: [],
  },
  reducers: {
    setWarehouseList: (state, action) => {
      state.list = action.payload;
      updateLocalStorage(state.list, entityKey);
    },
    addWarehouseStore: (state, action) => {
      state.list.push(action.payload)
      updateLocalStorage(state.list, entityKey);
    },
    updateWarehouseStore: (stateWarehouse, action) => {
      const { code, name, address, state, country, postalCode, zip, zip64, userId, lat, lng } = action.payload
      const warehouseFound = stateWarehouse.list.find(warehouse => String(warehouse.code) === String(code))
      if (warehouseFound) {
        warehouseFound.code = code
        warehouseFound.name = name
        warehouseFound.address = address
        warehouseFound.state = state
        warehouseFound.country = country
        warehouseFound.postalCode = postalCode
        warehouseFound.zip = zip
        warehouseFound.zip64 = zip64
        warehouseFound.userId = userId
        warehouseFound.lat = lat
        warehouseFound.lng = lng
      }
      updateLocalStorage(stateWarehouse.list, entityKey);
    },
    deleteWarehouseStore: (state, action) => {
      const warehouseFound = state.list.find(warehouse => String(warehouse.code) === String(action.payload))
      if (warehouseFound) {
        state.list.splice(state.list.indexOf(warehouseFound), 1)
        updateLocalStorage(state.list, entityKey);
      }
    },
  },
})

export const { setWarehouseList, addWarehouseStore, updateWarehouseStore, deleteWarehouseStore } = wareHouseSlice.actions

export const selectWarehouseList = (state) => state.warehouse.list;

// Get warehouses from state if localstorage(bd) is empty 
export const getWarehouses = () => (dispatch) => {
  const warehouses = getDataLocalStorage(entityKey);
  const data = warehouses?.length > 0 ? warehouses : WAREHOUSE_INITIALDATA
  dispatch(setWarehouseList(data));
}

// Add new warehouse with validations
export const addNewWarehouse = (newWarehouse) => async (dispatch, getState) => {
  const warehouseListState = selectWarehouseList(getState());
  const warehouseExist = warehouseListState.find(warehouse => String(warehouse.code) === String(newWarehouse.code))
  console.log(newWarehouse)
  // Validate duplicated warehouses
  if (warehouseExist) {
    newWarehouse.code = "Duplicated Error"
    return newWarehouse
  }

  // If you want to add the coords of the address discomment next lines and comment the line 75
  // // Fill warehouse with lat and lng if has
  // const warehouseWithCordinates = await getWarehouseWithCoordinates(newWarehouse)
  // dispatch(addWarehouseStore({...warehouseWithCordinates}))
  dispatch(addWarehouseStore(newWarehouse)) //MockTest
  return newWarehouse
}

// Update a warehouse and check if address has changed
export const updateWarehouse = (editedWarehouse) => async (dispatch, getState) => {
  const warehouseListState = selectWarehouseList(getState());
  const warehouseExist = warehouseListState.find(warehouse => String(warehouse.code) === String(editedWarehouse.code))

  if (warehouseExist) {
    // Fill warehouse with lat and lng if has and if address has changed or if lat and long are 0
    if (editedWarehouse.address !== warehouseExist.address 
      || editedWarehouse.state !== warehouseExist.state 
      || editedWarehouse.country !== warehouseExist.country 
      || editedWarehouse.postalCode !== warehouseExist.postalCode
      || (editedWarehouse.lat === 0 && editedWarehouse.lng === 0)
      ) {
        // If you want to update the coords when update the address discomment next lines //MockTest
        // const warehouseWithNewCordinates = await getWarehouseWithCoordinates(editedWarehouse) 
        // editedWarehouse.lat = warehouseWithNewCordinates.lat
        // editedWarehouse.lng = warehouseWithNewCordinates.lng
    }
    dispatch(updateWarehouseStore({...editedWarehouse}))
  } else {
    editedWarehouse.code = "Warehouse not exist for edit"
  }
  
  return editedWarehouse
}

// Fill lat and lng of warehouse object with the complete address
const getWarehouseWithCoordinates = async (warehouse) => {
  const street = warehouse.address?.replace(' ', "%20");
  const fullAddress = street + ",%20" + warehouse.state + ",%20" + warehouse.country + ",%20" + warehouse.postalCode
  const coordinates = await getCoordinatesByAddress(fullAddress)
  warehouse.lat = coordinates.lat
  warehouse.lng = coordinates.lng
  return warehouse
}

// Get Cordinates for warehouse address and set in warehouse state for add or update
export const getCoordinatesByAddress = async (fullAddress) => {
  try {
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${fullAddress}&key=${credentials.mapKey}`;
    const res = await fetch(url);
    const data = await res.json();

    return {
      lat: data?.results[0]?.geometry?.location.lat || 0,
      lng: data?.results[0]?.geometry?.location.lng || 0,
      fullAddress: fullAddress
    }
  } catch (error) {
    console.log("Error catch:", error);
  }
}
      
export default wareHouseSlice.reducer