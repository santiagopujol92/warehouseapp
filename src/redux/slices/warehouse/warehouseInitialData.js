export const WAREHOUSE_INITIALDATA = [
  {
    code: 1,
    name: 'Almacen Almacen',
    address: 'Alejandro Korn 3405',
    state: 'Ciudad de Córdoba, Provincia de Córdoba',
    country: 'Argentina',
    postalCode: '',
    zip: null,
    zip64: null,
    userId: 2,
    lat: -31.4508214,
    lng: -64.1958003
  },
  {
    code: 2,
    name: 'Almacen Pamela',
    address: 'Vecelli 737',
    state: 'Ciudad de Córdoba, Provincia de Córdoba',
    country: 'Argentina',
    postalCode: '',
    zip: null,
    zip64: null,
    userId: 2,
    lat: -31.4593032,
    lng: -64.1921072
  },
  {
    code: 3,
    name: 'Almacen Tio Chana',
    address: 'Gustavo Walther 3970',
    state: 'Ciudad de Córdoba, Provincia de Córdoba',
    country: 'Argentina',
    postalCode: '',
    zip: null,
    zip64: null,
    userId: 2,
    lat: -31.4592695,
    lng: -64.1928366
  },
  {
    code: 4,
    name: 'Almacen de Pato',
    address: 'José Amenabar 548',
    state: 'Ciudad de Córdoba, Provincia de Córdoba',
    country: 'Argentina',
    postalCode: '',
    zip: null,
    zip64: null,
    userId: 2,
    lat: -31.4537564,
    lng: -64.1935862
  },
  {
    code: 5,
    name: 'Almacen Celso Barrios',
    address: 'Celso Barrios 2265',
    state: 'Ciudad de Córdoba, Provincia de Córdoba',
    country: 'Argentina',
    postalCode: '',
    zip: null,
    zip64: null,
    userId: 2,
    lat: -31.458279,
    lng: -64.1676589
  }
];