export const USER_INITIALDATA = [
  {
    idUser: 1,
    email: "usertest@warehouse.com",
    username: "usertest",
    password: "usertest",
    type: 'user'
  },
  {
    idUser: 2,
    email: "useradmin@warehouse.com",
    username: "useradmin",
    password: "useradmin",
    type: 'admin'
  }
];