import { createSlice } from '@reduxjs/toolkit'
import { USER_INITIALDATA } from './userInitialData'
import { updateLocalStorage, getDataLocalStorage } from '../../localStorage'
const entityKey = 'users';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    userLogged: null,
    list: []
  },
  reducers: {
    setUserList: (state, action) => {
      state.list = action.payload;
      updateLocalStorage(state.list, entityKey);
    },
    setUserLogged: (state, action) => {
      state.userLogged = action.payload;
      updateLocalStorage(state.userLogged, 'userLogged');
    },
  },
})

export const { setUserList, setUserLogged } = userSlice.actions

export const selectUserList = (state) => state.user.list;

export const getUsers = () => (dispatch) => {
  const users = getDataLocalStorage(entityKey);
  const data = users?.length > 0 ? users : USER_INITIALDATA
  dispatch(setUserList(data));
}
      
export const setUserLogin = (username, password) => (dispatch, getState) => {
  const userListState = selectUserList(getState());
  const userFilter = userListState.find((item => username === item.username && password === item.password)) || null;
  dispatch(setUserLogged(userFilter));
}

export const setUserLogout = () => (dispatch) => {
  dispatch(setUserLogged(null));
}

export default userSlice.reducer