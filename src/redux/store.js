import { configureStore } from '@reduxjs/toolkit';
import userReducer from './slices/user/userSlice'
import warehouseReducer from './slices/warehouse/warehouseSlice'

export const store = configureStore({
  reducer: {
    user: userReducer,
    warehouse: warehouseReducer,
  },
});
