const updateLocalStorage = (data, key) => {
  localStorage.setItem(key, data != null ? JSON.stringify(data) : null);
}

const getDataLocalStorage = (key) => {
  return localStorage.getItem(key) != null 
    && localStorage.getItem(key) != undefined
    && localStorage.getItem(key).length > 0
  ? JSON.parse(localStorage.getItem(key)) 
  : [];
}

export { 
  updateLocalStorage, 
  getDataLocalStorage
}
