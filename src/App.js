import React from 'react';
import { Login } from './components/login/login';
import { Home } from './components/home/home';
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import { useDispatch } from 'react-redux'
import { setUserLogged } from './redux/slices/user/userSlice';
import { AppContextProvider } from './context.js';
import { getDataLocalStorage } from './redux/localStorage'

function App() {
  const dispatch = useDispatch()
  const userLogged = getDataLocalStorage("userLogged")
  dispatch(setUserLogged(userLogged));

  return (
    <AppContextProvider>
      <Router>
          <Routes>
            {!userLogged
              ? 
              <Route path="/" element={<Navigate to="login" />} /> 
              : 
              <Route path="/" element={<Navigate to="home" />} /> 
            }
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/home" element={<Home />} />
          </Routes>
      </Router>
    </AppContextProvider>
  );
}

export default App;