const credentials = {
  mapKey: "",
}
export default credentials

// Para utlizar la api es necesario requerir una key de Google Map
// To use the api it is necessary to require a Google Map key
// https://console.cloud.google.com/google/maps-apis/credentials

// habilitar servicio de Maps Javascript Api
// Enable Maps service javascript api
// https://console.cloud.google.com/apis/library/maps-backend.googleapis.com

// Se debe habilitar el servicio Geocode 
// The Geocode service must be enabled
// https://console.cloud.google.com/apis/library/geocoding-backend.googleapis.com

// Se debe habilitar el servicio Distances Matrix Api
// The Distances service must be enabled
//https://console.cloud.google.com/marketplace/product/google/distance-matrix-backend.googleapis.com

// Se debe habilitar el servicio Directions Api
// The Directions service must be enabled
//https://console.cloud.google.com/apis/library/directions-backend.googleapis.com
