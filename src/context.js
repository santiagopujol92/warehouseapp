import { useState, useContext, createContext, useMemo } from 'react';

//Context 
export const AppContext = createContext(null);

//Provider
export const AppContextProvider = ({ children }) => {
  const [openWarehouseAddUpdateModalForm, setOpenWarehouseAddUpdateModalForm] = useState(false);
  const [openCalculateNearestWarehouseModalForm, setOpenCalculateNearestWarehouseModalForm] = useState(false);
  const [notificationState, setNotificationState] = useState({
    open: false,
    type: "",
    message: "",
    timeOut: 2000,
  },);

  const values = useMemo(
    () => ({
      openWarehouseAddUpdateModalForm,
      setOpenWarehouseAddUpdateModalForm,
      openCalculateNearestWarehouseModalForm,
      setOpenCalculateNearestWarehouseModalForm,
      notificationState,
      setNotificationState
    }),
    [
      openWarehouseAddUpdateModalForm,
      openCalculateNearestWarehouseModalForm,
      notificationState
    ]
  ); // States que serán visibles en el contexto.

  // Interface donde será expuesto como proveedor y envolverá la App.
  return <AppContext.Provider value={values}>{children}</AppContext.Provider>;
};

// Custom hook.
export function useAppContext() {
  const context = useContext(AppContext);

  if (!context) {
    console.error('Error deploying App Context!!!');
  }

  return context;
}

export default useAppContext;
