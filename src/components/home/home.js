import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getWarehouses, deleteWarehouseStore } from '../../redux/slices/warehouse/warehouseSlice';
import { setUserLogout } from '../../redux/slices/user/userSlice';
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import WarehouseAddUpdateModalForm from '../warehouse/addEditModalForm';
import CalculateNearestWarehouseModalForm from '../warehouse/calculateNearestWarehouseModalForm';
import { useAppContext } from '../../context';
import NotificationToast from '../shared/notificationToast'

export function Home() {
  const warehouseState = useSelector((state) => state.warehouse)
  const userState = useSelector((state) => state.user)
  const dispatch = useDispatch()
  const navigate = useNavigate();
  const [warehouseElementEditList, setWarehouseElementEditList] = useState(null);

  const {
    setOpenWarehouseAddUpdateModalForm,
    setNotificationState,
    setOpenCalculateNearestWarehouseModalForm,
  } = useAppContext();

  const editWarehouseButton = (cell, row, rowIndex, formatExtraData) => {
    return (
      <button className="btn btn-primary"
        onClick={() => {
          setWarehouseElementEditList(row);
          setOpenWarehouseAddUpdateModalForm(true); 
        }}
      >
        Edit
      </button>
    );
  };

  const deleteWarehouseButton = (cell, row, rowIndex, formatExtraData) => {
    return (
      <button className="btn btn-danger"
        onClick={() => {
          dispatch(deleteWarehouseStore(row.code));
          setNotificationState({
            open: true,
            type: "success",
            message: "Warehouse deleted successfully",
            timeOut: 2000
          })
        }}
      >
        Delete
      </button>
    );
  };

  const downloadProductsWarehouseButton = (cell, row, rowIndex, formatExtraData) => {
    if (row.zip64 != null && row.zip64 !== '') {
      return (
        <a className="btn btn-warning float-center"
          href={row.zip64}
          target="_blank"
          rel="noreferrer"
          download
        >
          Download File
        </a>
      );
    } else {
      return <label>No file attached</label>
    }
  };

  const columns = [
    {
      id: 1,
      dataField: "code",
      text: "Code",
      sort: true,
    },
    {
      id: 2,
      dataField: "name",
      text: "Name",
      sort: true,
    },
    {
      id: 3,
      dataField: "address",
      text: "Address",
      sort: true,
    },
    {
      id: 4,
      dataField: "state",
      text: "State",
      sort: true,
    },
    {
      id: 5,
      dataField: "country",
      text: "Country",
      sort: true,
    },
    {
      id: 6,
      dataField: "postalCode",
      text: "Postal Code",
      sort: true,
    },
    {
      id: 7,
      dataField: "zip",
      text: "Products List",
      formatter: downloadProductsWarehouseButton,
    },
    {
      id: 8,
      dataField: "edit",
      text: "Edit",
      formatter: editWarehouseButton,
    },
    {
      id: 9,
      dataField: "delete",
      text: "Delete",
      formatter: deleteWarehouseButton,
    }
  ];

  useEffect(() => {  
    dispatch(getWarehouses())
    
    if (userState.userLogged === null) {
      navigate("/login")
    }
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, userState]);

  const logout = () => {
    dispatch(setUserLogout())
    navigate("/login")
  }

  const ModalFormAddEditComponent = () => {
    return (
      <WarehouseAddUpdateModalForm data={warehouseElementEditList}/>
    )
  }

  return (
    <>
      <div className="m-2">
        <NotificationToast />
        <h4 className="float-start text-primary"><b>WarehouseApp</b> - Warehouse List</h4>
        <button 
          type="button" 
          className="btn btn-secondary mb-2 float-end"
          onClick={() => logout()}
        >
          Logout <b>{userState.userLogged != null && userState.userLogged.username}</b>
        </button>

        <BootstrapTable
          bootstrap4
          keyField="id"
          data={warehouseState.list}
          columns={columns}
          pagination={paginationFactory({ sizePerPage: 5 })}
        />
        <button 
          type="button" 
          className="btn btn-success float-end mb-2"
          onClick={() => { setOpenWarehouseAddUpdateModalForm(true); setWarehouseElementEditList(null) }}
        >
          New Warehouse
        </button>
        { (userState.userLogged.type === "admin") && 
          <>
            <button 
              type="button" 
              className="btn btn-primary float-end mb-2 mx-2"
              onClick={() => { setOpenCalculateNearestWarehouseModalForm(true); }}
            >
              Calculate Nearest Warehouse
            </button>
            <CalculateNearestWarehouseModalForm warehousesList={warehouseState.list} />
          </>
        }
        <ModalFormAddEditComponent />
      </div>
    </>
  )
}

