import React, { useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { useAppContext } from '../../context';
import Map from '../map/map';
import { useSelector } from 'react-redux'
import { getCoordinatesByAddress } from '../../redux/slices/warehouse/warehouseSlice'; 

function WarehouseAddUpdateModalForm() {
  const {
    openCalculateNearestWarehouseModalForm,
    setOpenCalculateNearestWarehouseModalForm,
  } = useAppContext();

  const warehouseState = useSelector((state) => state.warehouse)

  const [errorValidationsForm, setErrorValidationsForm] = useState({});
  const [openMap, setOpenMap] = useState(false);
  const [initialCoords, setInitialCoords] = useState({});
  const [checkAddressButton, setCheckAddressButton] = useState(true);
  const [markersList, setMarkersList] = useState([]);
  const [stateForm, setStateForm] = useState({address: ''});

  const handleClose = () => {
    setOpenCalculateNearestWarehouseModalForm(false);
    setErrorValidationsForm({});
    setStateForm({ address: ''})
    setOpenMap(false)
    setCheckAddressButton(true)
  }

  function handleChange(e) {
      setStateForm({ ...stateForm, [e.target.name]: e.target.value });
  }

  const validateCalculateWarehouse = () => {
    let addressValidation = ""

    if (stateForm.address === "") 
      addressValidation = "To calculate nearest warehouse must need to complete 'address' input.";
    
    setErrorValidationsForm({
      addressValidation,
    });

    const canCalc = (addressValidation !== "") ? false : true;
    return canCalc;
  }

  const getAllMarkers = (list) => {
    return list != null && list.length > 0
    ? list.map((item) => { 
        return { 
          lat: item.lat, 
          lng: item.lng,
          fullAddress: (item.address + ", ") +
            (item.state !== "" ? ", " + item.state : "") +
            (item.country !== "" ? ", " + item.country : "") +
            (item.postalCode !== "" ? ", " + item.postalCode : "") 
        } 
      }) 
    : []
  }

  const calculate = async () => {
    const canCalc = validateCalculateWarehouse();
    if (!canCalc) {
      setOpenMap(false)
      setCheckAddressButton(true)
      return false
    }

    // If you want to get the coords of the search input in the moment discomment next lines and comment line 77
    // // Get cords for the address search
    // const initialCoordsResult = await getCoordinatesByAddress(stateForm.address)
    const initialCoordsResult = { lat: -31.44598, lng: -64.1606097} // MockTest
    setInitialCoords(initialCoordsResult)

    // Gets marks for map and set
    const allMarkers = getAllMarkers(warehouseState.list)
    if (allMarkers.length > 0) {
      setOpenMap(true)
      setMarkersList(allMarkers)
    }
  }

  return (
    <>
      <Modal show={openCalculateNearestWarehouseModalForm} size="xl" onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Calculate Nearest Warehouse</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form onSubmit={e => e.preventDefault()}>
          <fieldset>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="address">Address {errorValidationsForm.addressValidation !== "" &&
                <label className="text-danger">{"(" + (errorValidationsForm.addressValidation || "*")})</label>}
              </Form.Label>
              <Form.Control 
                id="address" 
                name="address"
                required={true}
                placeholder="Enter address"
                onChange={handleChange}
                value={stateForm.address} 
              />
            </Form.Group>
          </fieldset>
        </Form>
        {/* lista de puntos */}
        {openMap &&
          <Map 
            markersList={markersList}
            initialCoords={initialCoords}
            initialAddress={stateForm.address}
          />
        } 

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {checkAddressButton &&
            <Button variant="primary" onClick={() => { 
              calculate() 
              setCheckAddressButton(false)
            }}>
              Check Nearest Warehouses
            </Button>
          }
          {!checkAddressButton &&
            <Button variant="success" onClick={calculate}>
              Calculate Nearest Warehouse
            </Button>
          }
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default WarehouseAddUpdateModalForm;