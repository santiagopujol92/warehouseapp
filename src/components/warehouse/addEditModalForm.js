import React, { useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useSelector, useDispatch } from 'react-redux'
import { addNewWarehouse, updateWarehouse } from '../../redux/slices/warehouse/warehouseSlice';
import Form from 'react-bootstrap/Form';
import { useAppContext } from '../../context';
import NotificationToast from '../shared/notificationToast'
import Alert from 'react-bootstrap/Alert';

function WarehouseAddUpdateModalForm({data}) {
  const dispatch = useDispatch()
  const userState = useSelector((state) => state.user)

  const {
    openWarehouseAddUpdateModalForm,
    setOpenWarehouseAddUpdateModalForm,
    setNotificationState
  } = useAppContext();

  const [errorValidationsForm, setErrorValidationsForm] = useState({});

  const handleClose = () => {
    setOpenWarehouseAddUpdateModalForm(false);
  }

  const [stateForm, setStateForm] = useState({
    code: data?.code || '',
    name: data?.name || '',
    address: data?.address || '',
    state: data?.state || '',
    country: data?.country || '',
    postalCode: data?.postalCode || '',
    zip: data?.zip || '',
    zip64: data?.zip64 || '',
    userId: data?.userId || userState.userLogged.idUser,
    lat: data?.lat || 0,
    lng: data?.lng || 0,
  });
  
  function handleChange(e) {
    if (e.target.files) {
      getBase64(e.target.files[0]).then(base64 => {
        const fileName = e.target.files[0].name;
        setStateForm({ ...stateForm, 
          "zip": fileName,
          "zip64": base64
        });
      })
    } else {
      setStateForm({ ...stateForm, [e.target.name]: e.target.value });
    }
  }

  const validateWarehouse = () => {
    let codeValidation = ""
    let nameValidation = ""
    let addressValidation = ""
    let stateValidation = ""

    if (stateForm.code === "") 
      codeValidation = "A warehouse must have a code.";

    if (stateForm.name === "") 
      nameValidation = "A warehouse must have a name.";

    if (stateForm.address === "") 
      addressValidation = "A warehouse must have a address.";

    if (stateForm.state === "") 
      stateValidation = "A warehouse must have a state.";
    
    setErrorValidationsForm({
      codeValidation,
      nameValidation,
      addressValidation,
      stateValidation,
    });

    const canAddEdit = (codeValidation !== "" 
      || nameValidation !== "" 
      || addressValidation !== "" 
      || stateValidation !== "") ? false : true;
  
    return canAddEdit;
  }

  const saveWarehouse = async () => {
    try {
      const canAddEdit = validateWarehouse();
      if (!canAddEdit) return false
      
      // Add
      if (data == null) {
          const resultAdd = await dispatch(addNewWarehouse(stateForm))

          // Validate duplicated warehouses
          if (resultAdd.code === "Duplicated Error") {
            setNotificationState({
              open: true,
              type: "danger",
              message: "Warehouse with code " + stateForm.code + " already exist in the list.",
              timeOut: 10000
            })
            return false;
          }
          
      // Update
      } else {
        stateForm.code = data.code; 
        const resultEdit = await dispatch(updateWarehouse(stateForm))

        // Validate if warehouse exist
        if (resultEdit.code === "Warehouse not exist for edit") {
          setNotificationState({
            open: true,
            type: "danger",
            message: "Warehouse with code " + stateForm.code + " not exist in the list for edit.",
            timeOut: 10000
          })
          return false;
        }
      }
      handleClose()
      setNotificationState({
        open: true,
        type: "success",
        message: "Warehouse saved successfully",
        timeOut: 2000
      })
    } catch (error) {
      console.log(error)
      setNotificationState({
        open: true,
        type: "danger",
        message: "Error when save a warehouse. Error: " + error ,
        timeOut: 10000
      })
    }
  }

  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  return (
    <>
      <NotificationToast />
      <Modal show={openWarehouseAddUpdateModalForm} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{data != null ? 'Edit' : 'Add'} Warehouse</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form onSubmit={e => e.preventDefault()}>
          <fieldset>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="code">Code {errorValidationsForm.codeValidation !== "" &&
                <label className="text-danger">{"(" + (errorValidationsForm.codeValidation || "*")})</label>}
              </Form.Label>
              <Form.Control 
                disabled={data != null ? true : false }
                id="code" 
                name="code"
                required={true}
                placeholder="Enter code"
                onChange={handleChange}
                value={stateForm.code} 
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Name {errorValidationsForm.nameValidation !== "" &&
                <label className="text-danger">{"(" + (errorValidationsForm.nameValidation || "*")})</label>}
              </Form.Label>
              <Form.Control 
                id="name" 
                name="name"
                required={true}
                placeholder="Enter name"
                onChange={handleChange}
                value={stateForm.name} 
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="address">Address {errorValidationsForm.addressValidation !== "" &&
                <label className="text-danger">{"(" + (errorValidationsForm.addressValidation || "*")})</label>}
              </Form.Label>
              <Form.Control 
                id="address" 
                name="address"
                required={true}
                placeholder="Enter address"
                onChange={handleChange}
                value={stateForm.address} 
              />

            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="state">State {errorValidationsForm.stateValidation !== "" &&
                <label className="text-danger">{"(" + (errorValidationsForm.stateValidation || "*")})</label>}
              </Form.Label>
              <Form.Control 
                id="state" 
                name="state"
                required={true}
                placeholder="Enter state"
                onChange={handleChange}
                value={stateForm.state} 
              />

            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="country">Country</Form.Label>
              <Form.Control 
                id="country" 
                name="country"
                placeholder="Enter country"
                onChange={handleChange}
                value={stateForm.country} 
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="postalCode">Postal Code</Form.Label>
              <Form.Control 
                id="postalCode" 
                name="postalCode"
                placeholder="Enter postal code"
                onChange={handleChange}
                value={stateForm.postalCode} 
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="zip">Product List (.zip)</Form.Label>
              <Form.Control 
                id="zip" 
                name="zip"
                type="file"
                placeholder="Enter file for download"
                accept='.zip'
                onChange={handleChange}
              />
              {(stateForm.zip64 != null && stateForm.zip64 !== '') &&
                <>
                <Form.Label className="text-success mt-2">File Attached: <b>{stateForm.zip}</b></Form.Label>
                <button className="btn btn-danger float-end mt-1"
                  onClick={() => setStateForm({ ...stateForm, zip: null, zip64: null })}
                >
                  <b>Delete File</b>
                </button>
                </>
              }
            </Form.Group>
          </fieldset>
        </Form>
        {(data != null && stateForm.lat === 0 && stateForm.lng === 0 && stateForm.address !== "") &&
          <Alert className="mt-2" variant={"warning"}>
            <h6>Atention! You need update this warehouse becouse has been added automatically and not have the address locations configurated for map</h6>
          </Alert> 
        }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={saveWarehouse}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default WarehouseAddUpdateModalForm;