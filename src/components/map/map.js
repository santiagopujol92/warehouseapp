import credentials from '../../credentials'
import React, { useState, useEffect } from "react";
import {
  useJsApiLoader,
  GoogleMap,
  Marker,
  DirectionsRenderer,
  // DistanceMatrixService
} from "@react-google-maps/api";

function Map({initialCoords,  markersList, initialAddress}) {

  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: credentials.mapKey,
  });

  // const [map, setMap] = useState(null);
  const [directionsResponse, setDirectionsResponse] = useState(null);
  const [markers, setMarkers] = useState(markersList || []);
  const [markersWithDistances, setMarkersWithDistances] = useState([]);

  // Get Array of markers with distances between initialAddress and markers
  function getAndSetMarkersWithDistances(allMarkers) {
    if (allMarkers.length > 0 && initialAddress !== "" && initialCoords != null) {
      let markersWithDistancesTemp = []
      const matrix = new window.google.maps.DistanceMatrixService();

      allMarkers.forEach(element => { 
        matrix.getDistanceMatrix({
          origins: [new window.google.maps.LatLng(Number(initialCoords.lat), Number(initialCoords.lng))],
          destinations: [new window.google.maps.LatLng(Number(element.lat), Number(element.lng))],
          travelMode: window.google.maps.TravelMode.DRIVING,
        }, function(response, status) {
          if (status === "OK") {
            markersWithDistancesTemp.push({
              distance: response?.rows[0].elements[0].distance.value || 0,
              duration: response?.rows[0].elements[0].duration.value || 0,
              originAddresses: initialAddress,
              destinationAddresses: element.fullAddress,
              lat: element.lat,
              lng: element.lng
            })
          }
        });
      });
      setMarkersWithDistances(markersWithDistancesTemp)
    }
  }

  // Get Array of markers with distances between initialAddress and markers FAKE
  function getAndSetMarkersWithDistancesFake(allMarkers) {
    if (allMarkers.length > 0 && initialAddress !== "" && initialCoords != null) {
      let markersWithDistancesTemp = []
      let index = 0
      allMarkers.forEach(element => { 
        index++
        const rndm = Math.random(10)
        markersWithDistancesTemp.push({
          distance: index * rndm,
          duration: index * rndm,
          originAddresses: initialAddress,
          destinationAddresses: element.fullAddress,
          lat: element.lat,
          lng: element.lng
        })    
      });
      setMarkersWithDistances(markersWithDistancesTemp)
    }
  }

  // Draw Direction Route Between 2 points address (initialAddress and best Nearest)
  async function setRouteBetweenPoints(origin, destination) {
    if (origin !== "" && destination !== "" && window.google !== undefined) {
      const directionsService = new window.google.maps.DirectionsService();
      const results = await directionsService.route({
        origin: origin,
        destination: destination,
        travelMode: window.google.maps.TravelMode.DRIVING,
      });
      return results;    
    }
  }

  useEffect(() => {
    const asyncUseEffect = async () => {
      if (markersList.length > 0) {
        // If you want to get the real distances from the warehouses using Distances Api 
        // comment next line and discomment line 92
        getAndSetMarkersWithDistancesFake(markersList) //MockTest
        // getAndSetMarkersWithDistances(markersList)

        // Set and Order Top 3 nearest markers from initialAddress
        if (markersWithDistances?.length > 0) {
          const topNearestMarkers = markersWithDistances
            .sort((a, b) => a.distance - b.distance)
            .slice(0, 3);
            setMarkers(topNearestMarkers) 

            const bestNearestMarker = topNearestMarkers[0];
            if (bestNearestMarker) {
              const resultDirections = await setRouteBetweenPoints(bestNearestMarker.originAddresses, bestNearestMarker.destinationAddresses)
              setDirectionsResponse(resultDirections);    
            }
        }
      }
    }
    asyncUseEffect()

  }, [initialCoords, markersList])

  function MarkerNearestList() {
    if (markers.length > 0) {
        const markerList = markers.map((coords, index) =>
          <Marker position={coords} key={index} />
        );
        return (
          <>
            {markerList}
          </>
        )
    }
  }
  
  return isLoaded ? (
    <>
      <GoogleMap
        center={initialCoords}
        zoom={13}
        mapContainerStyle={{ width: "100%", height: "400px" }}
        options={{
          zoomControl: true,
          streetViewControl: false,
          mapTypeControl: false,
          fullscreenControl: true,
        }}
        // onLoad={(map) => setMap(map)}
      >
        <Marker 
          position={initialCoords}
          icon={"http://maps.google.com/mapfiles/ms/icons/yellow.png"}
          label={"My direction"}
         />
        <MarkerNearestList />
        {directionsResponse && (
          <DirectionsRenderer directions={directionsResponse} />
        )}
      </GoogleMap>
    </>
  ) : (
    <></>
  );
}

export default Map;