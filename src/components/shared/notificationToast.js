import { useEffect } from 'react';
import { useAppContext } from '../../context';
import Toast from 'react-bootstrap/Toast';
import ToastContainer from 'react-bootstrap/ToastContainer';

const NotificationToast = () => {  

  const {
    notificationState,
    setNotificationState
  } = useAppContext();

  //type: 'success', 'warning', 'danger', 'info', 'primary'
  const { type, open, message, timeOut } = notificationState

  const closeToast = () => setNotificationState({ ...notificationState, open: false });
  const today = new Date();
  const time = today.getHours() + ":" + (today.getMinutes().toString().length == 1 ? "0" + today.getMinutes() : today.getMinutes());

	useEffect(() => {
    if (open) {
      setTimeout(() => {
        closeToast();
      }, timeOut);
    }
	}, [notificationState])

	return (
		<>
      <ToastContainer position={"bottom-start"} className={"p-2"}>
        <Toast show={open} onClose={closeToast} className={"bg-" + type}>
            <Toast.Header>
              <strong className={"me-auto"}>Notification</strong>
              <small>{time} hs</small>
            </Toast.Header>
              <Toast.Body className="text-white">{message}</Toast.Body>
          </Toast>
      </ToastContainer>
		</>
	)
}

export default NotificationToast
